import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  useWindowDimensions,
} from 'react-native';

const ImageDetail = (props) => {
  const [isLoading, setIsLoading] = useState(true);
  const dimensions = useWindowDimensions();
  const imageURL = props.route.params.imageURL;

  return (
    <View style={styles.MainContainer}>
      {isLoading && (
        <View style={styles.activityIndicator}>
          <ActivityIndicator size="large" color={'#000000'} />
        </View>
      )}
      <Image
        source={{
          uri: `${imageURL}&w=${dimensions.width * dimensions.scale}&h=${
            dimensions.height * dimensions.scale
          }&q=80&fit=fill&fill=blur`,
        }}
        style={styles.image}
        onLoad={() => {
          setIsLoading(false);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    marginTop: 1,
  },
  image: {
    flex: 1,
    borderRadius: 10,
    width: '100%',
    height: '100%',
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ImageDetail;
