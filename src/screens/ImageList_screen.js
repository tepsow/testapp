import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {fetchImages} from '../redux_thunk/actions/images_actions';
import {useSelector, useDispatch} from 'react-redux';

const ImageList = (props) => {
  const images = useSelector((state) => state.images);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [isRefreshing, setIsRefreshing] = useState(false);

  const loadImages = useCallback(() => {
    setIsRefreshing(true);

    dispatch(fetchImages())
      .then(() => {
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('Error: ', error);
      });

    setIsRefreshing(false);

    return () => {
      setIsLoading(true);
    };
  }, [dispatch]);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', loadImages);

    return () => {
      unsubscribe();
    };
  }, [loadImages]);

  useEffect(() => {
    loadImages();
  }, [dispatch, loadImages]);

  const selectItemHandler = (url) => {
    props.navigation.navigate('Detail', {
      imageURL: url,
    });
  };

  FlatListItemSeparator = () => {
    return <View style={styles.separatorContainer} />;
  };

  if (isLoading) {
    return <ActivityIndicator size="large" color={'#000000'} />;
  }

  return (
    <View style={styles.MainContainer}>
      <FlatList
        onRefresh={loadImages}
        refreshing={isRefreshing}
        data={images}
        ItemSeparatorComponent={FlatListItemSeparator}
        keyExtractor={({id}, index) => id}
        renderItem={({item}) => (
          <View style={styles.ItemContainer}>
            <TouchableOpacity onPress={() => selectItemHandler(item.urls.raw)}>
              <Image
                source={{uri: `${item.urls.raw}&w=300&h=300&q=80`}}
                style={styles.image}
              />
            </TouchableOpacity>

            <View style={styles.textContainer}>
              <Text style={styles.text}>Title : {item.alt_description}</Text>
              <Text style={styles.text}>Author: {item.user.name}</Text>
            </View>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    margin: 5,
    paddingTop: StatusBar.height || 0,
  },

  ItemContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  textContainer: {
    flex: 1,
    flexDirection: 'column',
  },

  separatorContainer: {
    flex: 1,
    height: 1,
    width: '100%',
    backgroundColor: '#000',
  },

  image: {
    flex: 1,
    width: 200,
    height: 200,
    borderRadius: 10,
    margin: 10,
  },

  text: {
    flex: 1,
    width: '100%',
    textAlignVertical: 'center',
    color: '#000',
    padding: 10,
  },
});

export default ImageList;
