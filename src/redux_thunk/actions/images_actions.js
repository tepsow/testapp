import {
  FETCHING_REQUEST,
  FETCHING_SUCCESS,
  FETCHING_FAILURE,
} from './actions_constants';

const client_id =
  'cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

export const fetchingImagesRequest = () => {
 
  return { 
    type: FETCHING_REQUEST,
  };

};

export const fetchingImagesSuccess = (json) => {
  
  return {
  type: FETCHING_SUCCESS,
  payload: json,
  };

};

export const fetchingImagesFailure = (error) => {
 return {
  type: FETCHING_FAILURE,
  payload: error,
 };
 
};

export const fetchImages = () => {
  
  return dispatch => {
    dispatch(fetchingImagesRequest());
    
    return fetch(`https://api.unsplash.com/photos/?client_id=${client_id}`)
      .then((response) => response.json())
      .then((json) => {
        
        return dispatch(fetchingImagesSuccess(json));
       
      })
      .catch((err) => dispatch(fetchingImagesFailure(err)));
  };
};
