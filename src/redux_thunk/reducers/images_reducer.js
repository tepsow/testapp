import {
  FETCHING_REQUEST,
  FETCHING_SUCCESS,
  FETCHING_FAILURE,
} from '../actions/actions_constants';

const initialState = {
  isFetching: false,
  errorMessage: '',
  images: [],
};

const imagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case FETCHING_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
      };
    case FETCHING_SUCCESS:
      return {
        ...state,
        isFetching: false,
        images: action.payload,
      };
    default:
      return state;
  }
};

export default imagesReducer;
