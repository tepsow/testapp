import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import ImageList from '../screens/ImageList_screen';
import ImageDetail from '../screens/ImageDetail_screen';

const Stack = createStackNavigator();

const StackNavigator = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Images"
        screenOptions={{
          headerStyle: {
            backgroundColor: 'powderblue',
          },
        }}>
        <Stack.Screen
          name="Images"
          component={ImageList}
          options={{title: 'Images'}}
        />
        <Stack.Screen
          name="Detail"
          component={ImageDetail}
          options={{title: 'Detail screen'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default StackNavigator;
