import React, {Component} from 'react';

import StackNavigator from './src/navigation/StackNavigator';

import store from './src/redux_thunk/store';
import {Provider} from 'react-redux';

const App = () => {
  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  );
};

export default App;
